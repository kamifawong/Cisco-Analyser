
!! PLEASE VERIFY BEFORE REAL APPLICATION !!

%% NOT APPLICABLE ON non-Cisco products, or Cisco Nexus products, Cisco AP & Cisco controllers


##  Guide  ##

  1. Copy the content of a log file into "__input.txt".
  2. Run the "analyse" program. (For convenience, better to use cmd)
  3. The program will end with a generated file named "__output.txt".
     Outputs on the terminal reveal conditions. All [-1] flags reveal normal completeion.
  4. Copy the content of "__output.txt" into the excel file.

  
##  Contents dealed with ##

  Hostname , Interface , ( ) , ( ) , ( ) , Port Up/down , Port type ,
  Interface IP addr , Interface IP subnet , VLAN , Port-Channel , Duplex , Speed ,
  ( ) , ( ) , cdp Destination Hostname , cdp Destination Platform , cdp Destination Interface , cdp Destination VLAN , cdp Destination IP Address

  % Items with ( ) brackets will print an empty column. %
  
  
##  Contents not dealed with  ##

  Device Model , Device MAC Address , Interface MAC Address(es) ,
  Interface Description
 