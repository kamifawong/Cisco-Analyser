
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>

#define BUFFER_SIZE 5120
#define int_num 1024


	void replace_char(char *str, char org, char final){    // To replace an unwanted character.
                                                           // final = '\0' for erasing that character.
    char *src, *dst;
    for (src = dst = str; *src != '\0'; src++) {
        *dst = *src;
        if (*dst != org) dst++;
		else if (final != '\0'){
		    *dst = final;
		    dst++;
		}
		}
    
    *dst = '\0';
}

	char* short_int(char* interface) {
		
		char _int[10] = "";
		int pointer;
		strncpy(_int, interface, 2);
		interface = strcat(_int, strpbrk(interface, "0123456789"));
		
		return interface;
		
	}


int main( int argc, char** argv ){

    char *delimiters = " \t\r\n\a\v\f";
    char buffer[BUFFER_SIZE];
    char *last_token;

    FILE *input_file;
    FILE *fw1;
	
	
	char hostname[30] = "";
	char interface[int_num][20] = {""};
	char int_status[int_num][20] = {""};
	char int_duplex[int_num][20] = {""};
	char int_speed[int_num][20] = {""};
	char int_ip_addr[int_num][20] = {""};
	char int_ip_subnet[int_num][20] = {""};
	
	char access_vlan[int_num][10] = {""};
	char voice_vlan[int_num][10] = {""};
	char trunk_native_vlan[int_num][20] = {""};
	char trunk_allowed_vlan[int_num][300] = {""};
	char private_vlan[int_num][200] = {""};
	char mode[int_num][50] = {""};
	char channel_group[int_num][10] = {""};
	
	char cdp_device[int_num][30] = {""};
	char cdp_interface[int_num][20] = {""};
	char cdp_address[int_num][20] = {""};
	char cdp_platform[int_num][50] = {""};
	char cdp_native_vlan[int_num][10] = {""};

	char __cdp_device[int_num][30] = {""};
	char __cdp_local_interface[int_num][20] = {""};
	char __cdp_interface[int_num][20] = {""};
	char __cdp_address[int_num][20] = {""};
	char __cdp_platform[int_num][50] = {""};
	char __cdp_native_vlan[int_num][10] = {""};
	
	char tmp_cdp_device[30] = "";
	char tmp_cdp_local_interface[20] = "";
	char tmp_cdp_interface[20] = "";
	char tmp_cdp_address[20] = "";
	char tmp_cdp_platform[50] = "";
	char tmp_cdp_native_vlan[10] = "";
	
	char tmp_int[20] = "";
	
	
	int aa, bb;
	int count;
	
	int flag_show_interface_status = 0;
	int flag_show_interface_des = 0;
	
	int flag_show_cdp = 0;
	int flag_show_cdp_detail = 0;
	
	int flag_show_run = 0;

	input_file = fopen("__input.txt", "r");
	fw1 = fopen("__output.txt", "w");
	
    if((input_file == NULL)||(fw1 == NULL)){

        fprintf(stderr, "Unable to open file! \n" );

    }else{
	
		printf("START ----- \n");
			
        while (fgets(buffer, BUFFER_SIZE, input_file) != NULL){
			
            last_token = strtok(buffer, delimiters);

			if (flag_show_cdp == 1) flag_show_cdp = 0;
			if (flag_show_cdp_detail == 1) flag_show_cdp_detail = 0;
			if (flag_show_run == 1) flag_show_run = 0;
			if (flag_show_interface_status == 1) flag_show_interface_status = 0;
			if (flag_show_interface_des == 1) flag_show_interface_des = 0;

            while (last_token != NULL) {

				if (strstr(last_token, "#show") != 0) {
					
					if ((strlen(hostname) == 0) && (strlen(last_token) > 0)) {
						strncpy(hostname, last_token, strcspn(last_token, "#"));
						printf("Hostname: %s\n\n", hostname);
					}
					
					if ((flag_show_run != 0) && (flag_show_run != 1)) {
						flag_show_run = -1;
					} else flag_show_run = 1;
					
					if ((flag_show_interface_status != 0) && (flag_show_interface_status != 1)) {
						flag_show_interface_status = -1;
					} else flag_show_interface_status = 1;
					
					if ((flag_show_interface_des != 0) && (flag_show_interface_des != 1)) {
						flag_show_interface_des = -1;
					} else flag_show_interface_des = 1;
					
					if ((flag_show_cdp != 0) && (flag_show_cdp != 1)) {
						flag_show_cdp = -1;
					} else flag_show_cdp = 1;
					
					if ((flag_show_cdp_detail != 0) && (flag_show_cdp_detail != 1)) {
						flag_show_cdp_detail = -1;
					} else flag_show_cdp_detail = 1;
					
				}
					
					////  show run
					
					if (flag_show_run == 3) {
	
						if ((strcmp(last_token, "!") == 0)) {
							if (strlen(mode[count]) == 0)  strcpy(mode[count], "-");
							count = count + 1;
							flag_show_run = 2;
							break;
						}
						
						//  ip address & subnet
						if (strcmp(last_token, "ip") == 0) {
							
							last_token = strtok(NULL, delimiters);
							if (last_token == NULL || strcmp(last_token, "address") != 0)  break;
							
							last_token = strtok(NULL, delimiters);
							if (last_token == NULL)  break;
							strcpy(int_ip_addr[count], last_token);
							last_token = strtok(NULL, delimiters);
							strcpy(int_ip_subnet[count], last_token);
						}
						
						//  duplex
						if (strcmp(last_token, "duplex") == 0) {
							
							last_token = strtok(NULL, delimiters);
							if (last_token == NULL)  break;
							
							if (strlen(int_duplex[count]) == 0) {
								strcpy(int_duplex[count], last_token);
							}
						}
						
						
						//  speed
						if (strcmp(last_token, "speed") == 0) {
							
							last_token = strtok(NULL, delimiters);
							if (last_token == NULL)  break;
							
							if (strlen(int_speed[count]) == 0) {
								strcpy(int_speed[count], last_token);
							}
						}
						
						
						//  switchport
						if (strcmp(last_token, "switchport") == 0) {
							
							last_token = strtok(NULL, delimiters);
							if (last_token == NULL)  break;
							
							if (strcmp(last_token, "access") == 0) {
								last_token = strtok(NULL, delimiters);
								last_token = strtok(NULL, delimiters);
								strcpy(access_vlan[count], last_token);
							}
							
							if (strcmp(last_token, "voice") == 0) {
								last_token = strtok(NULL, delimiters);
								last_token = strtok(NULL, delimiters);
								strcpy(voice_vlan[count], last_token);
							}
							
							if (strcmp(last_token, "trunk") == 0) {
								last_token = strtok(NULL, delimiters);
								if (strcmp(last_token, "native") == 0) {
									last_token = strtok(NULL, delimiters);
									last_token = strtok(NULL, delimiters);
									strcpy(trunk_native_vlan[count], last_token);
								}
								if (strcmp(last_token, "allowed") == 0) {
									last_token = strtok(NULL, delimiters);
									last_token = strtok(NULL, delimiters);
									if (strcmp(last_token, "add") == 0)  last_token = strtok(NULL, delimiters);
									if (strlen(trunk_allowed_vlan[count]) > 0)  strcat(trunk_allowed_vlan[count], ",");
									strcat(trunk_allowed_vlan[count], last_token);
										
								}
								
							}
							
							if (strcmp(last_token, "private-vlan") == 0) {
								last_token = strtok(NULL, delimiters);
							//	printf("--%s--\n", last_token);
								last_token = strtok(NULL, delimiters);
							//	printf("--%s--\n", last_token);
								strcpy(private_vlan[count], last_token);
								last_token = strtok(NULL, delimiters);
							//	printf("--%s--\n", last_token);
								while (last_token != NULL) {
									strcat(private_vlan[count], ",");
									strcat(private_vlan[count], last_token);
									last_token = strtok(NULL, delimiters);
								//	printf("--%s--\n", last_token);
								}
								break;
							}
							
							if (strcmp(last_token, "mode") == 0) {
								last_token = strtok(NULL, delimiters);
							//	printf("[[%s]]\n", last_token);
								strcpy(mode[count], last_token);
								last_token = strtok(NULL, delimiters);
							//	printf("[[%s]]\n", last_token);
								while (last_token != NULL) {
									strcat(mode[count], " ");
									strcat(mode[count], last_token);
									last_token = strtok(NULL, delimiters);
								//	printf("[[%s]]\n", last_token);
								}
								break;
							}
							
						}
						
						//  channel-group
						else if (strcmp(last_token, "channel-group") == 0) {
							last_token = strtok(NULL, delimiters);
							if (last_token == NULL)  break;
							strcpy(channel_group[count], last_token);
						}
						
					}
					
					if (flag_show_run == 2) {
						
						if (strcmp(last_token, "interface") == 0) {
							last_token = strtok(NULL, delimiters);
						//	printf("interface [%s]\n", last_token);
							if (last_token == NULL)  break;
							
							if ((strstr(last_token, "Ethernet") != NULL) || (strstr(last_token, "Serial") != NULL) || (strstr(last_token, "Embedded-Service-Engine") != NULL))
								flag_show_run = 3;
							if (strstr(last_token, "Vlan") != NULL) {
								printf(" ** show run END **\n\n");
								flag_show_run = -1;
							}
							
						}
						
					}
					
					if (flag_show_run == 1) {
						last_token = strtok(NULL, delimiters);
						if (strstr(last_token, "ru") != NULL) {
							printf("start show run\n");
							count = 0;
							flag_show_run = 2;
						}
					}
					
					
					
					////  show interface des - have all interfaces
					
					if (flag_show_interface_des == 2) {

					//	printf("**start show interface des**\n");

						memset(tmp_int, 0, strlen(tmp_int));
						
							if (strstr(last_token, "Te") != NULL) {
								strcpy(tmp_int, "Ten ");
							} else if (strstr(last_token, "Gi") != NULL) {
								strcpy(tmp_int, "Gig ");
							} else if (strstr(last_token, "Fa") != NULL) {
								strcpy(tmp_int, "Fas ");
							} else if (strstr(last_token, "Et") != NULL) {
								strcpy(tmp_int, "Eth ");
							} else if (strstr(last_token, "Se") != NULL) {
								strcpy(tmp_int, "Ser ");
							} else if (strstr(last_token, "Em") != NULL) {
								strcpy(tmp_int, "Emb ");
							} else	
								break;
							
							strcat(tmp_int, strpbrk(last_token, "0123456789"));
							
							strcpy(interface[count], tmp_int);
							last_token = strtok(NULL, delimiters);
							
							if (strlen(int_status[count]) == 0) {
								if (strstr(last_token, "up") != NULL)
									strcpy(int_status[count], "up");
								else if (strstr(last_token, "down") != NULL)
									strcpy(int_status[count], "down");
								else if (strstr(last_token, "admin") != NULL) {
									last_token = strtok(NULL, delimiters);
									if (strstr(last_token, "down") != NULL)
										strcpy(int_status[count], "admin down");
								}
								
							}
							
							count = count + 1;
					}
					
					
					////  show interface status - some interfaces are hidden
					
					if (flag_show_interface_status == 2) {
						
						memset(tmp_int, 0, strlen(tmp_int));

						if (strstr(last_token, "Te") != NULL) {
							strcpy(tmp_int, "Ten ");
						} else if (strstr(last_token, "Gi") != NULL) {
							strcpy(tmp_int, "Gig ");
						} else if (strstr(last_token, "Fa") != NULL) {
							strcpy(tmp_int, "Fas ");
						} else if (strstr(last_token, "Et") != NULL) {
							strcpy(tmp_int, "Eth ");
						} else if (strstr(last_token, "Se") != NULL) {
							strcpy(tmp_int, "Ser ");
						} else if (strstr(last_token, "Em") != NULL) {
							strcpy(tmp_int, "Emb ");
						} else	
							break;

						strcat(tmp_int, strpbrk(last_token, "0123456789"));
						
						for (count = 0; count < int_num; count = count + 1) {
							if ((strcmp(interface[count], tmp_int) == 0) && (strlen(tmp_int) != 0)) {
							
								last_token = strtok(NULL, delimiters);
								
								while ((strstr(last_token, "connected") == NULL) && (strstr(last_token, "notconnect") == NULL)
									&& (strstr(last_token, "disabled") == NULL) && (strstr(last_token, "inactive") == NULL)) {
									last_token = strtok(NULL, delimiters);
								}

								strcpy(int_status[count], last_token);
						
							//	printf("%s - %s\n", interface[count], int_status[count]);
						
								last_token = strtok(NULL, delimiters);
								if (((strlen(mode[count]) == 0) || (strcmp(mode[count], "-") == 0)) && (strcmp(last_token, "routed") == 0))
									strcpy(mode[count], "routed");
								last_token = strtok(NULL, delimiters);
								strcpy(int_duplex[count], last_token);
								last_token = strtok(NULL, delimiters);
								strcpy(int_speed[count], last_token);
							
							}
						}


					}
						
						
					
					if (((flag_show_interface_status == 1) || (flag_show_interface_des == 1)) && (strcmp(last_token, "interface") == 0)) {

						last_token = strtok(NULL, delimiters);
			
						if (strstr(last_token, "stat") != NULL) {
							flag_show_interface_status = 2;
						}
							
						if (strstr(last_token, "des") != NULL) {
							flag_show_interface_des = 2;
							count = 0;
						}
						
					}

				////  show cdp
				
				if (flag_show_cdp == 3) {
					
						if (strlen(tmp_cdp_device) == 0) {
							if (strstr(last_token, ".") != NULL) {
								strncpy(tmp_cdp_device, last_token, strlen(last_token) - strlen(strstr(last_token, ".")));
							} 
							else if (strstr(last_token, "(") != NULL) {
								strncpy(tmp_cdp_device, last_token, strlen(last_token) - strlen(strstr(last_token, "(")));
							} 
							else strcpy(tmp_cdp_device, last_token);
						} else if (strlen(tmp_cdp_local_interface) == 0) {
							strcpy(tmp_cdp_local_interface, last_token);
							last_token = strtok(NULL, delimiters);
							strcat(strcat(tmp_cdp_local_interface, " "), last_token);
						} else if ((strstr(last_token, "Eth") != NULL) || (strstr(last_token, "Fas") != NULL) || (strstr(last_token, "Gig") != NULL)
							|| (strstr(last_token, "Ten") != NULL) || (strstr(last_token, "Port") != NULL)
						|| (strstr(last_token, "Ser") != NULL) || (strstr(last_token, "Emb")!= NULL)) {
							strcpy(tmp_cdp_interface, last_token);
							last_token = strtok(NULL, delimiters);
							strcat(strcat(tmp_cdp_interface, " "), last_token);

							strcpy(__cdp_device[count], tmp_cdp_device);
							strcpy(__cdp_local_interface[count], tmp_cdp_local_interface);
							strcpy(__cdp_interface[count], tmp_cdp_interface);
							
							memset(tmp_cdp_device, 0, sizeof(tmp_cdp_device));
							memset(tmp_cdp_local_interface, 0, sizeof(tmp_cdp_local_interface));
							memset(tmp_cdp_interface, 0, sizeof(tmp_cdp_interface));
							
						//	printf("dev - [%s]\n", __cdp_device[count]);
						//	printf("local int - [%s]\n", __cdp_local_interface[count]);
						//	printf("cdp int - [%s]\n\n", __cdp_interface[count]);
							
							count = count + 1;
							
						}
						
				}
				
				
				if (flag_show_cdp == 2) {
					if (strcmp(last_token, "Port") == 0) {
						last_token = strtok(NULL, delimiters);
						memset(tmp_cdp_device, 0, 0);
						memset(tmp_cdp_local_interface, 0, 0);
						memset(tmp_cdp_interface, 0, 0);
						flag_show_cdp = 3;
						count = 0;
					}
				}
				
				
				////  show cdp detail
				
				if (flag_show_cdp_detail == 2) {
					
					if ((strstr(last_token, "Total") != NULL) || (strstr(last_token, hostname) != NULL)
						|| ((strlen(tmp_cdp_device) > 0) && (strstr(last_token, "--------") != NULL))) {

						for (aa = 0; aa < int_num; aa++) {
							if ((strlen(__cdp_device[aa]) > 0) && (strstr(__cdp_device[aa], tmp_cdp_device) != NULL)
								&& (strstr(__cdp_local_interface[aa], tmp_int) != NULL)) {
									
								strcpy(__cdp_address[aa], tmp_cdp_address);
								strcpy(__cdp_platform[aa], tmp_cdp_platform);
								strcpy(__cdp_native_vlan[aa], tmp_cdp_native_vlan);
							}
						}
						
						memset(tmp_cdp_device, 0, strlen(tmp_cdp_device));
						memset(tmp_int, 0, strlen(tmp_int));
						memset(tmp_cdp_address, 0, strlen(tmp_cdp_address));
						memset(tmp_cdp_platform, 0, strlen(tmp_cdp_platform));
						memset(tmp_cdp_native_vlan, 0, strlen(tmp_cdp_native_vlan));
						
						
						if ((strstr(last_token, "Total") != NULL) || (strstr(last_token, hostname) != NULL)) {
							flag_show_cdp_detail = -1;
							printf("** show cdp detail - END **\n\n");
						}
						
					}
					
					// Device ID
					if ((strlen(tmp_cdp_device) == 0) && (strcmp(last_token, "Device") == 0)) {
						last_token = strtok(NULL, delimiters);
						if ((last_token != NULL) && (strcmp(last_token, "ID:") == 0)) {
							last_token = strtok(NULL, delimiters);
							if (strstr(last_token, ".") !=  NULL) {
								strncpy(tmp_cdp_device, last_token, strlen(last_token) - strlen(strstr(last_token, ".")));
							}
							else if (strstr(last_token, "(") !=  NULL) {
								strncpy(tmp_cdp_device, last_token, strlen(last_token) - strlen(strstr(last_token, "(")));
							}
							else strcpy(tmp_cdp_device, last_token);
						}
					}
					
					// IP address
					if ((strlen(tmp_cdp_address) == 0) && (strcmp(last_token, "IP") == 0)) {
						last_token = strtok(NULL, delimiters);
						if (strcmp(last_token, "address:") == 0) {
							last_token = strtok(NULL, delimiters);
							if (last_token != NULL) {
								strcpy(tmp_cdp_address, last_token);
							}
						}
					}
					
					//  cdp Platform
					if ((strlen(tmp_cdp_platform) == 0) && (strcmp(last_token, "Platform:") == 0)) {
						
						last_token = strtok(NULL, delimiters);
						if (strstr(last_token, ",") != NULL) {
							strncpy(tmp_cdp_platform, last_token, strlen(last_token) - 1);
						} else {
							strcpy(tmp_cdp_platform, last_token);
						}
						
						while (strstr(last_token, ",") == NULL) {
							strcat(tmp_cdp_platform, " ");
							last_token = strtok(NULL, delimiters);
							if (strstr(last_token, ",") != NULL) {
								char str[20] = "";
								strncpy(str, last_token, strlen(last_token) - 1);
								strcat(tmp_cdp_platform, str);
							} else {
								strcat(tmp_cdp_platform, last_token);
							}
						}
						
					}
					
					//  cdp Interface
					if ((strlen(tmp_int) == 0) && (strstr(last_token, "Interface:") != NULL)) {
						
						last_token = strtok(NULL, delimiters);
						
						if (strstr(last_token, "TenGigabitEthernet") != NULL) {
							strcpy(tmp_int, "Ten ");
						} else if (strstr(last_token, "GigabitEthernet") != NULL) {
							strcpy(tmp_int, "Gig ");
						} else if (strstr(last_token, "FastEthernet") != NULL) {
							strcpy(tmp_int, "Fas ");
						} else if (strstr(last_token, "Ethernet") != NULL) {
							strcpy(tmp_int, "Eth ");
						} else if (strstr(last_token, "Serial") != NULL) {
							strcpy(tmp_int, "Ser ");
						} else if (strstr(last_token, "Embedded") != NULL) {
							strcpy(tmp_int, "Emb ");
						}
					
						strncat(tmp_int, strpbrk(last_token, "0123456789"), strlen(strpbrk(last_token, "0123456789")) - 1);
					
					}
					
					//  Native VLAN
					
					if ((strlen(tmp_cdp_native_vlan) == 0) && (strstr(last_token, "Native") != NULL)) {
						last_token = strtok(NULL, delimiters);
						if (strstr(last_token, "VLAN") == NULL)  break;
						last_token = strtok(NULL, delimiters);
						strcpy(tmp_cdp_native_vlan, last_token);
					}
					
				}
				
				if (((flag_show_cdp == 1) || (flag_show_cdp_detail == 1)) && (strcmp(last_token, "cdp") == 0)) {
					last_token = strtok(NULL, delimiters);
					if (strstr(last_token, "nei") == NULL) {
						if (flag_show_cdp == 1)  flag_show_cdp = -1;
						if (flag_show_cdp_detail == 1)  flag_show_cdp_detail = -1;
						break;
					}
					last_token = strtok(NULL, delimiters);
					if ((last_token != NULL) && (strstr(last_token, "det") != NULL)) {
						flag_show_cdp_detail = 2;
					} else {
						flag_show_cdp = 2;
					}
				}
		
				last_token = strtok(NULL, delimiters);
			}
			
				
            }
			
        if (ferror(input_file) ){
            perror( "The following error occurred" );
        }

        

    }
	
	
	
	int i, j;
	for (i = 0; i < int_num; i++) {
		
		for (j = 0; j < int_num; j++) {
			if (strcmp(interface[i], __cdp_local_interface[j]) == 0) {
				strcpy(cdp_device[i], __cdp_device[j]);
				strcpy(cdp_interface[i], __cdp_interface[j]);
				strcpy(cdp_address[i], __cdp_address[j]);
				strcpy(cdp_platform[i], __cdp_platform[j]);
				strcpy(cdp_native_vlan[i], __cdp_native_vlan[j]);
			}
		}
		
		if ((strlen(interface[i]) + strlen(int_status[i]) + strlen(int_duplex[i]) + strlen(int_speed[i])) > 0) {
			
			char tmpstr[200] = "";
			memset(tmpstr, 0, strlen(tmpstr));
			
			if (strcmp(mode[i], "access") == 0) {
				
				if (strlen(access_vlan[i]) > 0) {
					strcat(tmpstr, access_vlan[i]);
				}
				
				if (strlen(voice_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "voice ");
					strcat(tmpstr, voice_vlan[i]);
				}
				if (strlen(trunk_allowed_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "trunk_allowed ");
					strcat(tmpstr, trunk_allowed_vlan[i]);
				}
				if (strlen(trunk_native_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "trunk_native ");
					strcat(tmpstr, trunk_native_vlan[i]);
				}
				if (strlen(private_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "private_vlan ");
					strcat(tmpstr, private_vlan[i]);
				}
			}
			else if (strcmp(mode[i], "trunk") == 0) {
				
				if (strlen(trunk_allowed_vlan[i]) > 0) {
					strcat(tmpstr, trunk_allowed_vlan[i]);
				}
				
				if (strlen(trunk_native_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "native ");
					strcat(tmpstr, trunk_native_vlan[i]);
				}
				if (strlen(voice_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "voice ");
					strcat(tmpstr, voice_vlan[i]);
				}
				if (strlen(access_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "access ");
					strcat(tmpstr, access_vlan[i]);
				}
				if (strlen(private_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "private_vlan ");
					strcat(tmpstr, private_vlan[i]);
				}

			} 
			else if (strstr(mode[i], "private-vlan") != NULL) {
				
				if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, private_vlan[i]);
				
				if (strlen(access_vlan[i]) > 0) {
					strcat(tmpstr, "access ");
					strcat(tmpstr, access_vlan[i]);
				}
				if (strlen(voice_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "voice ");
					strcat(tmpstr, voice_vlan[i]);
				}
				if (strlen(trunk_allowed_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "trunk_allowed ");
					strcat(tmpstr, trunk_allowed_vlan[i]);
				}
				if (strlen(trunk_native_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "trunk_native ");
					strcat(tmpstr, trunk_native_vlan[i]);
				}
			}
			else if ((strcmp(mode[i], "-") == 0) || (strlen(mode[i]) == 0)){
				
				if (strlen(access_vlan[i]) > 0) {
					strcat(tmpstr, "access ");
					strcat(tmpstr, access_vlan[i]);
				}
				if (strlen(voice_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "voice ");
					strcat(tmpstr, voice_vlan[i]);
				}
				if (strlen(trunk_allowed_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "trunk_allowed ");
					strcat(tmpstr, trunk_allowed_vlan[i]);
				}
				if (strlen(trunk_native_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "trunk_native ");
					strcat(tmpstr, trunk_native_vlan[i]);
				}
				if (strlen(private_vlan[i]) > 0) {
					if (strlen(tmpstr) > 0)  strcat(tmpstr, ", ");
					strcat(tmpstr, "private_vlan ");
					strcat(tmpstr, private_vlan[i]);
				}
				
			}
			
			  //  Hostname , Interface , Interface_status , Switchport_mode
			fprintf(fw1, "%s\t%s\t\t\t\t%s\t%s", hostname, interface[i], int_status[i], mode[i]);
			  //  IP_addr , IP_subnet , Vlan_str , Port_channel_group , Interface_duplex , Interface_speed
			fprintf(fw1, "\t%s\t%s\t%s\t%s\t%s\t%s", int_ip_addr[i], int_ip_subnet[i], tmpstr, channel_group[i], int_duplex[i], int_speed[i]);
			  //  CDP_neighbor_device , CDP_neighbor_platform , CDP_neighbor_interface , CDP_neighbor_native_vlan , CDP_neighbor_IP_addr
			fprintf(fw1, "\t\t\t%s\t%s\t%s\t%s\t%s\n", cdp_device[i], cdp_platform[i], cdp_interface[i], cdp_native_vlan[i], cdp_address[i]);

		}
	}

	
	
	fclose(input_file);
    fclose(fw1);
	
	printf("show cdp nei - [%d]\n", flag_show_cdp);
	printf("show cdp nei detail - [%d]\n", flag_show_cdp_detail);
	printf("show run - [%d]\n", flag_show_run);
	printf("show interface des - [%d]\n", flag_show_interface_des);
	printf("show interface status - [%d]\n", flag_show_interface_status);
	
	if (flag_show_cdp + flag_show_cdp_detail + flag_show_run + flag_show_interface_des + flag_show_interface_status == -5)
		printf("\n  ----- Normally Completed\n\n");
	else  printf("\n  xxxxx Not normally completed\n\n");

    return 0;

}